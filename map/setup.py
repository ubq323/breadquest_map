from setuptools import find_packages, setup

setup(
    name = "breadquest_map",
    version = "0.0.2",
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires = [
        "flask", "redis", "pillow"
    ]
)
