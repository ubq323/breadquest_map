import io, math

import redis
r = redis.Redis()

from PIL import Image, ImageDraw

MAPTILESIZE = 32 # game tiles per map tile
IMAGESIZE = 256  # size of map tile image in pixels
SMALLTILESIZE = IMAGESIZE//MAPTILESIZE # size of game tile in pixels


def draw_small_tile(draw, x0, y0, x1, y1, tile):
    colours = "red", "orange", "yellow", "green", "teal", "blue", "purple", "grey"
    if tile is None:
        return draw.rectangle((x0, y0, x1, y1), fill=(128,128,128))
    # ascii: 33-127
    # blank: 128
    # blocks: 129 - 136
    # trail: 137 - 144
    # 145 flour
    # 146 water
    # 147 powder
    # 148 bread
    # 149 oven
    # 150 hospital
    tile = int(tile)

    if 129 <= tile <= 136: # blocks
        draw.rectangle((x0, y0, x1, y1), fill=colours[tile-129])
    elif 137 <= tile <= 144: # trails
        draw.ellipse((x0+3, y0+3, x1-3, y1-3), fill=colours[tile-137])
    elif 33 <= tile <= 127: # text
        draw.text((x0+2, y0-1), chr(tile), fill="black")
    elif 149 <= tile <= 150:
        draw.rectangle((x0, y0, x1, y1), fill=(64, 64, 64), outline="black", width=1)
        if tile == 149: # oven
            draw.line([ (x0+2, y0+2), (x1-2, y0+2) ], fill="orange", width=1)
            draw.line([ (x0+2, y1-3), (x1-2, y1-3) ], fill="orange", width=1)
        else: # hospital
            draw.line([(x0+3, y0), (x0+3, y1)], fill="red", width=2)
            draw.line([(x0, y0+3), (x1, y0+3)], fill="red", width=2)

def get_map_tile(mapx, mapy):
    map_tile_name = f"map:{mapx}:{mapy}"
    cached_tile = r.get(map_tile_name)
    if cached_tile is not None:
        return io.BytesIO(cached_tile)
    else:
        map_tile = generate_map_tile(mapx, mapy)
        r.set(f"map:{mapx}:{mapy}", map_tile.getvalue())
        return map_tile

def invalidate_on_new_data(pos, size):
    # used by the recieve code to work out when to invalidate cached map tiles.
    map_start_x = pos['x'] // MAPTILESIZE
    map_start_y = pos['y'] // MAPTILESIZE

    num = math.ceil(size / MAPTILESIZE)
    for x in range(map_start_x, map_start_x+num):
        for y in range(map_start_y, map_start_y+num):
            r.delete(f"map:{x}:{y}")


def generate_map_tile(mapx, mapy):
    image = Image.new("RGB", (IMAGESIZE,IMAGESIZE), color=(255,255,255))
    draw = ImageDraw.Draw(image)

    tile_start_x = mapx * MAPTILESIZE
    tile_start_y = mapy * MAPTILESIZE

    for tile_dx in range(MAPTILESIZE):
        for tile_dy in range(MAPTILESIZE):
            tile_x = tile_start_x + tile_dx
            tile_y = tile_start_y + tile_dy

            tile = r.get(f"tile:{tile_x}:{tile_y}")

            x0 = tile_dx * SMALLTILESIZE
            y0 = tile_dy * SMALLTILESIZE
            x1 = (tile_dx+1) * SMALLTILESIZE
            y1 = (tile_dy+1) * SMALLTILESIZE

            draw_small_tile(draw, x0, y0, x1, y1, tile)

    fp = io.BytesIO()
    image.save(fp, "PNG")
    fp.seek(0)

    return fp
