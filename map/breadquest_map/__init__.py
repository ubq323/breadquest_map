from flask import (
    Flask, send_file, render_template, request, make_response
)

from breadquest_map.recieve import recieve_tiles
from breadquest_map.map_tiles import get_map_tile

def create_app():

    app = Flask(__name__)

    @app.route("/tile/<x>/<y>.png")
    def tile(x,y):
        return send_file(get_map_tile(int(x),int(y)), mimetype="image/png")


    def recieve():
        if request.method == "OPTIONS":
            resp = make_response("")
            resp.headers["Access-Control-Allow-Origin"] = "https://ostracodapps.com:2626"
            resp.headers["Access-Control-Allow-Methods"] = "PUT"
            resp.headers["Access-Control-Allow-Headers"] = "Content-Type"
            return resp
        else:
            resp = make_response({"s":True}, 200)
            resp.headers["Access-Control-Allow-Origin"] = "https://ostracodapps.com:2626"
            recieve_tiles(request.json)
            return resp
    recieve.provide_automatic_options = False
    recieve.methods = ["PUT", "OPTIONS"]
    app.add_url_rule("/recieve", "recieve", recieve)



    return app
