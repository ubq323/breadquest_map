import math
import redis
from breadquest_map.map_tiles import invalidate_on_new_data

r = redis.Redis()


# handles putting the recieved data into the db
def recieve_tiles(payload):
    pipe = r.pipeline()

    size = payload['size']
    pos = payload['pos']
    tile_list = payload['tiles']

    # tile_list is a size * size array of tiles, with pos at the top left, row-by-row

    start_x = pos['x']
    start_y = pos['y']

    idx = 0
    for dy in range(size):
        for dx in range(size):
            tile = tile_list[idx]
            idx += 1
            tile_name = f"tile:{start_x + dx}:{start_y + dy}"
            pipe.set(tile_name, tile)

    pipe.execute()

    invalidate_on_new_data(pos, size) # delete any cached tiles modified
