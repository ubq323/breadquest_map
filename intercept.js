recieve_url = "https://breadquest.xn--vtgil-jua.world/api/recieve";

window.addGetTilesCommand = function() {
    gameUpdateCommandList.push({
        commandName: "getTiles",
        size: 50 // override size to the maximum allowed
    });
}

setTilesIndex = 0;

function handleTiles(command) {
    setTilesIndex = setTilesIndex + 1;
    if (setTilesIndex >= 40) {
        setTilesIndex = 0;

        var payload = {
            size: command.size,
            pos: command.pos,
            tiles: command.tileList
        };

        var xhr = new XMLHttpRequest();
        xhr.open("PUT", recieve_url);
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.send(JSON.stringify(payload));
    }
}

originalPSTC = performSetTilesCommand;
window.performSetTilesCommand = function(c) {
    handleTiles(c);
    originalPSTC(c);
}
