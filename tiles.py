# a test script to draw tile information onto the screen
# basically to test i understood the format correctly

import json

with open("tiles.json") as f:
    tiles = json.load(f)


def chunks(source, length):
    # yields successive length-sized chunks from source
    for i in range(0, len(source), length):
        yield source[i : i+length]


# ascii: 33-127
# blank: 128
# blocks: 129 - 136
# trail: 137 - 144
# 145 flour
# 146 water
# 147 powder
# 148 bread
# 149 oven
# 150 hospital

colours = [
    # "red", "orange", "yellow", "green", "teal", "blue", "purple", "grey"
       1,     3,        8,        2,       6,      4,      5,        7
]

tilemap = {
    128: ' ',

    # using letters for now
    145: 'F',
    146: "W",
    147: "P",
    148: "B",
    149: "O",
    150: "H"
}

def col(c, s):
    return f"\033[3{colours[c]}m{s}\033[0m"


def getc(i):
    if i <= 127:
        return chr(i)
    elif 129 <= i <= 136:
        return col(i-129, "█")
    elif 137 <= i <= 144:
        return col(i-137, "·")
    else:
        return tilemap[i]

for row in chunks(tiles, 45):
    print("".join(getc(i) for i in row))
